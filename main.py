"""
RedPitaya FFT
https://www.wiredsense.com/tutorials/measurement-of-voltage-noise-density-with-a-red-pitaya
"""
#necessary modules to import
import redpitaya_scpi as scpi
import matplotlib.pyplot as plt
import numpy as np
from scipy import integrate
import pandas as pd

#create the instance of the scipy server
#TODO put in your correct Red Pitayas IP-Adress here
rp_s = scpi.scpi('192.168.1.53')

#set the prescaler according to the frequency range you want to observe.
#the number 8192 will give you a freq. range from 1 Hz to 7 kHz
prescaler = 1024
rp_s.tx_txt('ACQ:DEC ' + str(prescaler))
rp_s.tx_txt('ACQ:DEC?')
#make sure the prescaler was set
print('Prescaler set to: ' + str(rp_s.rx_txt()))

#prepare some arrays
buff_ffts = pd.DataFrame()
buffs = pd.DataFrame()

#the number of averages will determine the quality of your noise spectrum 
#keep the number around 10 in the beginning not to wait too long and increase it if you are sure everything works
numOfAverages = 100

#the offset takes a certain number of spectra in the beginning. 
#Sometimes the Red Pitaya produces trash in the first spectra
offset = 5

#do the acquisitions and save it in the computers memory (not on the Red Pitaya).
for i in range(1,numOfAverages+offset):
    if i % 50 == 0:
        print(i)
    rp_s.tx_txt('ACQ:START')
    rp_s.tx_txt('ACQ:TRIG NOW')
    rp_s.tx_txt('ACQ:SOUR1:DATA?')
    buff_string = ''
    buff_string = rp_s.rx_txt()
    buff_string = buff_string.strip('{}\n\r').replace(" ", "").split(',')
    #display(buff_string)
    buffs[i] = list(map(float, buff_string))
    buff_ffts[i] = (np.fft.fft(buffs[i])/16384)**2
 
"""
xVect = np.arange(0,prescaler/125e6*16384,prescaler/125e6)
'''Plot the 5th buffer to see if everything is fine'''
fig, ax = plt.subplots(dpi=72*2)
ax.plot(xVect*1000,buffs[5]*1000, label='', lw=1)
ax.set_title('Red Pitaya Test2');
ax.set_xlabel('time [ms]')
ax.set_ylabel('voltage [mV]')
plt.show()
"""

#determine the timestep to calculate the frequency axis
timestep = prescaler/125e6

#get the frequencies
freq = pd.Series(np.fft.fftfreq(16384, d=timestep))
freq2plot = freq[1:int(freq.size/2)]

#get the first usable spectrum into the result variable
result = 2*np.abs(buff_ffts[1+offset][1:int(freq.size/2)])/numOfAverages

for i in range(2+offset,numOfAverages+offset):
    result = result + 2*np.abs(buff_ffts[i][1:int(freq.size/2)])/numOfAverages
resultV2perHz = pd.DataFrame({'Freq': freq2plot, 'FFT': result})
resultV2perHz.to_csv('Circuit_V2_per_Hz.dat')
result_uVrms = 1e6*np.sqrt(integrate.trapz(result, freq2plot))
print("\nV_rms = % 5.3f uV" %(result_uVrms))

#plot results
fig, ax = plt.subplots(dpi=72*2)
ax.plot(freq2plot, np.sqrt(result), label='Noise Floor\n' + str(round(result_uVrms,3)) + ' uV RMS', lw=1)
ax.set_title('Noise Amplitude Spectrum (' + str(numOfAverages) + ' AVG)')
ax.set_xlabel('Freq [Hz]')
ax.set_ylabel('Noise Ampl. [V/$\sqrt{Hz}$]')
ax.set_yscale('log')
ax.set_xscale('log')
ax.grid(which='both', axis='both')
ax.set_xlim(1, 1e4)
ax.legend(loc='upper right', bbox_to_anchor=(1, 1), prop={'size': 8})
plt.show()