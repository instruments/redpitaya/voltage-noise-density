# This script receives redpitayas ADC data connected as SCPI server and calculates voltage noise density
# Based on a WiredSense Tutorial https://www.wiredsense.com/tutorials/measurement-of-voltage-noise-density-with-a-red-pitaya
# Dependency: matplotlib; numpy; scipy; pandas 
